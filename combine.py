import numpy as np
#import matplotlib.pyplot as plt



a = np.loadtxt("./tmp/rtcb1.out")
b=np.loadtxt("./tmp/outputS_avg.out")

norm_a = np.trapz(a[30:-2,1],a[30:-2,0])
norm_b = np.trapz(b[5:-6,1],b[5:-6,0])

a[:,1:-1] = a[:,1:-1]/norm_a
b[:,1:-1] = b[:,1:-1]/norm_b


a[:,0] = 180-a[:,0]
a = a[a[:,0].argsort()]



theta = a[:,0]
cb = a[:,1:17]
rt = a[:,17:]

#print(cb.shape)
#print(rt.shape)

diff = cb-rt



#plt.plot(theta,rt[:,0])
#plt.plot(theta,cb[:,0])

#plt.show()

#np.savetxt(diff)


theta2 = b[:,0]





#print(theta)
#print(theta2)

newtheta = np.concatenate((theta,theta2),axis=0)
newtheta = np.unique(newtheta)
newtheta = np.sort(newtheta)
#print(newtheta)
#print(b)

newb = np.zeros((len(newtheta),16))
newdiff = np.zeros((len(newtheta),16))

for i in range(0,16):
    newb[:,i] = np.interp(newtheta, theta2, b[:,i+1])
    newdiff[:,i] = np.interp(newtheta, theta, diff[:,i])


#print newdiff[:,0]


c= newb+newdiff
#print(newdiff[:,0])
#plt.plot(newdiff[:,0])
#plt.show()

#plt.plot(theta,diff[:,0])
#plt.plot(theta,cb[:,0])
#plt.plot(theta,rt[:,0])
#plt.plot(theta2,b[:,1])
#plt.plot(newtheta,newb[:,0])
#plt.plot(newtheta,c[:,0])
#plt.show()


#plt.plot(newtheta,-newb[:,1]/newb[:,0])
#plt.plot(newtheta,-c[:,1]/c[:,0])
#plt.show()

newtheta.shape = (len(newtheta),1)

c = np.concatenate((newtheta,c),axis=1)
np.savetxt("tmp/final.out",c)
