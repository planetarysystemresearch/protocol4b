import numpy as np
import funcs as g
import subprocess


old_params = g.get_params()
new_params = {}


def run_pmdec(readfrom,output):
    ##invtool
    line_count=0
    with open(readfrom) as f:
        for i, l in enumerate(f):
            line_count=line_count+1
    print(str(int(line_count)))
    out = subprocess.Popen(["./pmdec",readfrom,str(int(line_count)-1),"F"])
    out.wait()

    A=np.loadtxt("pmdec0.out")
    B=np.loadtxt("smdec0.out")

    B = np.delete(B,0,axis=1)
    C = np.concatenate((A,B),axis=1)
    np.savetxt("tmp/"+output,C)


new_params["INPUT_SMALL"] = "pnew_small.in"
new_params["INPUT_LARGE"] = "pnew_large.in"

ws = (float(old_params["SET_SMALL_PERCENTAGE"])*0.01)/float(old_params["SET_SMALL_MFP"])
wl = (1-float(old_params["SET_SMALL_PERCENTAGE"])*0.01)/float(old_params["SET_LARGE_MFP"])



new_params["SET_MEAN_FREE_PATH"] = str(1/(ws+wl))


new_params["PERCENTAGE_SMALL"] = str(ws/(ws+wl))
new_params["PERCENTAGE_LARGE"] = str(1)

run_pmdec(old_params["SET_READ_SMALL_FNAME"],new_params["INPUT_SMALL"])
run_pmdec(old_params["SET_READ_LARGE_FNAME"],new_params["INPUT_LARGE"])


def remove_nan(fname):
     data = np.genfromtxt(fname, delimiter=" ", filling_values=np.nan)
     nans = np.where(np.isnan(data))
     data[nans[0],nans[1]] = (data[nans[0]+1,nans[1]]+data[nans[0]-1,nans[1]])*0.5
     np.savetxt(fname,data)

remove_nan("tmp/"+new_params["INPUT_SMALL"])
remove_nan("tmp/"+new_params["INPUT_LARGE"])

##rtcb
g.fill("run_rtcb_proto.in","tmp/run_rtcb.in",[old_params,new_params])





