#!/bin/bash -l

#SBATCH -J rtcb
#SBATCH -t SET_RTCB_TIME
#SBATCH -o step_3_a_o
#SBATCH -e step_3_a_e
#SBATCH --mem-per-cpu=SET_RTCB_MEM
#SBATCH -p parallel
#SBATCH -n SET_NCORES_HERE
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=END
#SBATCH --mail-user=SET_EMAIL_HERE

module load gcc openmpi openblas hdf5-serial
cd CURRENT_WRK_DIR
srun --mpi=pmi2 ../rtcbSphere_MPI 1 run_rtcb.in

