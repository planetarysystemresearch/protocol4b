#!bin/bash

git clone git@bitbucket.org:planetarysystemresearch/rtcb_public.git rtcb
cd rtcb
mkdir obj
mkdir mod
git checkout multiparticle
make 
cp rtcbSphere ../
cd ..

git clone git@bitbucket.org:planetarysystemresearch/m_inversion_tool.git invtool
cd invtool/src
gfortran pmdec.f90 -o ../../pmdec
cd ../..


mkdir tmp

